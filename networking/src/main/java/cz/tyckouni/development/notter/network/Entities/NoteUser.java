package cz.tyckouni.development.notter.network.Entities;


import java.io.Serializable;

/**
 * Entity class for NoteUser information
 * Contains information about noteUser such as id, noteUser userName etc.
 */
public class NoteUser implements Serializable {
    private Long id;
    private String userName;
    private String emailAddress;
    private String passwordHash;

    private NoteUser(Builder builder) {
        this.id = builder.id;
        this.userName = builder.userName;
        this.emailAddress = builder.emailAddress;
        this.passwordHash = builder.passwordHash;
    }

    public static class Builder {
        private Long id;
        private String userName;
        private String emailAddress;
        private String passwordHash;

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder userName(String userName) {
            this.userName = userName;
            return this;
        }

        public Builder passwordHash(String passwordHash) {
            this.passwordHash = passwordHash;
            return this;
        }

        public Builder emailAddress(String emailAddress) {
            this.emailAddress = emailAddress;
            return this;
        }

        public NoteUser build() {
            return new NoteUser(this);
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NoteUser noteUser = (NoteUser) o;

        if (id != null ? !id.equals(noteUser.id) : noteUser.id != null) return false;
        if (userName != null ? !userName.equals(noteUser.userName) : noteUser.userName != null) return false;
        if (emailAddress != null ? !emailAddress.equals(noteUser.emailAddress) : noteUser.emailAddress != null) return false;
        return passwordHash != null ? passwordHash.equals(noteUser.passwordHash) : noteUser.passwordHash == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (emailAddress != null ? emailAddress.hashCode() : 0);
        result = 31 * result + (passwordHash != null ? passwordHash.hashCode() : 0);
        return result;
    }
}
