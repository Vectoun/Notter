package cz.tyckouni.development.notter.network;

import java.io.Serializable;

/**
 * Created by Vectoun on 12. 5. 2017.
 */
public enum ServerResponse implements Serializable {
    SUCCESS,
    FAILURE,
    ERROR
}
