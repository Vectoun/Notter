package cz.tyckouni.development.notter.network.common;

import java.sql.SQLException;

/**
 * Exception class for service failure
 */
public class ServiceFailureException extends SQLException {

    public ServiceFailureException(String message) {
        super(message);
    }

    public ServiceFailureException(String message, Throwable cause) {
        super(message, cause);
    }
}
