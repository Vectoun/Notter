package cz.tyckouni.development.notter.network.common;

/**
 * Exception class for illegal entity exception
 */
public class IllegalEntityException extends RuntimeException {

    public IllegalEntityException(String message) {
        super(message);
    }

    public IllegalEntityException(String message, Throwable cause) {
        super(message, cause);
    }
}
