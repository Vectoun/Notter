package cz.tyckouni.development.notter.network.common;

/**
 * Exception class for user name validation duplicate
 */
public class DuplicateUserNameEception extends ValidationException {

    public DuplicateUserNameEception(String message) {
        super(message);
    }

    public DuplicateUserNameEception(String message, Throwable throwable) {
        super(message, throwable);
    }
}
