package cz.tyckouni.development.notter.network.Entities;

import java.io.Serializable;
import java.util.List;

/**
 * Entity class for Note page
 * Contains information about note page such as dimensions etc.
 */
public class NotePage implements Serializable {
    private Long id;
    private String color;
    private NoteUser noteUser;
    private List<Note> notes;
    private int width;
    private int height;

    private NotePage(Builder builder) {
        this.id = builder.id;
        this.color = builder.color;
        this.noteUser = builder.noteUser;
        this.notes = builder.notes;
        this.width = builder.width;
        this.height = builder.height;
    }

    public static class Builder {
        private Long id;
        private String color;
        private NoteUser noteUser;
        private List<Note> notes;
        private int width;
        private int height;

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder color(String color) {
            this.color = color;
            return this;
        }

        public Builder noteUser(NoteUser noteUser) {
            this.noteUser = noteUser;
            return this;
        }

        public Builder notes(List<Note> notes) {
            this.notes = notes;
            return this;
        }

        public Builder width(int width) {
            this.width = width;
            return this;
        }

        public Builder height(int height) {
            this.height = height;
            return this;
        }

        public NotePage build() {
            return new NotePage(this);
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public NoteUser getNoteUser() {
        return noteUser;
    }

    public void setNoteUser(NoteUser noteUser) {
        this.noteUser = noteUser;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }


    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NotePage notePage = (NotePage) o;

        if (width != notePage.width) return false;
        if (height != notePage.height) return false;
        if (id != null ? !id.equals(notePage.id) : notePage.id != null) return false;
        if (color != null ? !color.equals(notePage.color) : notePage.color != null) return false;
        if (noteUser != null ? !noteUser.equals(notePage.noteUser) : notePage.noteUser != null) return false;
        return notes != null ? notes.equals(notePage.notes) : notePage.notes == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (color != null ? color.hashCode() : 0);
        result = 31 * result + (noteUser != null ? noteUser.hashCode() : 0);
        result = 31 * result + (notes != null ? notes.hashCode() : 0);
        result = 31 * result + width;
        result = 31 * result + height;
        return result;
    }
}
