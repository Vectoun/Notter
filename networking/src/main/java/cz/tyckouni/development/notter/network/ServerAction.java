package cz.tyckouni.development.notter.network;

import java.io.Serializable;

/**
 * Enum for server actions
 */
public enum ServerAction implements Serializable {
    NEW_NOTE_PAGE,
    UPDATE_NOTE_PAGE,
    SEND_DATA,
    VERIFY_NOTE_USER,
    SIGN_UP_USER,
    DOWNLOAD_PAGE,
    DELETE_PAGE,
    CREATE_NEW_USER,
    USER_EXISTS,
    CLOSE
}
