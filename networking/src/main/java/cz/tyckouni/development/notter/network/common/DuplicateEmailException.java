package cz.tyckouni.development.notter.network.common;

/**
 * Exception class for email validation exception
 */
public class DuplicateEmailException extends ValidationException {

    public DuplicateEmailException(String message) {
        super(message);
    }

    public DuplicateEmailException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
