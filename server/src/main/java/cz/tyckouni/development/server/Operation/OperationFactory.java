package cz.tyckouni.development.server.Operation;

import cz.tyckouni.development.notter.network.ServerAction;
import cz.tyckouni.development.server.Request;

/**
 * Class for creating instances of {@link Operation}
 */
public class OperationFactory {

    public static Operation fromRequestAndAction(Request request, ServerAction action){
        Operation operation;

        switch (action) {
            case USER_EXISTS:
                operation = new UserExistsOperation(request);
                break;
            case VERIFY_NOTE_USER:
                operation = new VerifyUserOperation(request);
                break;
            case DELETE_PAGE:
                operation = new RemoveNotePageOperation(request);
                break;
            case SIGN_UP_USER:
                operation = new VerifyUserOperation(request);
                break;
            case SEND_DATA:
                operation = new SendNotePagesOperation(request);
                break;
            case DOWNLOAD_PAGE:
                operation = new SendNotePageOperation(request);
                break;
            case NEW_NOTE_PAGE:
                operation = new CreateNotePageOperation(request);
                break;
            case UPDATE_NOTE_PAGE:
                operation = new UpdateNotePageOperation(request);
                break;
            case CREATE_NEW_USER:
                operation = new CreateNoteUserOperation(request);
                break;
            default:
                throw new IllegalStateException("Unknown Server action " + action);
        }

        return operation;
    }
}
