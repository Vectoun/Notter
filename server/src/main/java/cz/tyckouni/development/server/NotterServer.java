package cz.tyckouni.development.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Server class
 */
public class NotterServer {

    private final static Logger log = LoggerFactory.getLogger(NotterServer.class);

    private Properties serverProperties;

    public NotterServer() {
        serverProperties = new Properties();
        try {
            serverProperties.load(getClass().getResourceAsStream("/server-config.properties"));
        } catch (IOException e) {
            log.error("Failed to load config file");
            throw new RuntimeException("Failed to load config file", e);
        }
    }

    private final Executor executor = Executors.newCachedThreadPool();

    @SuppressWarnings("InfiniteLoopStatement") // added because of main infinite loop
    private void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(Integer.parseInt(serverProperties.getProperty("SERVER_PORT")));

            while (true) {
                final Socket socket = serverSocket.accept();

                Thread connectionThread = new Thread(new ConnectionHandler(socket, DBManager.getPGDataSource()));
                connectionThread.setDaemon(true);

                executor.execute(connectionThread);
            }
        } catch (IOException e) {
            log.error("An IOException occurred in main run method", e);
            throw new RuntimeException("An IOException occurred in main run method", e);
        }
    }

    public static void main(String[] args) {
        new NotterServer().run();
    }
}
