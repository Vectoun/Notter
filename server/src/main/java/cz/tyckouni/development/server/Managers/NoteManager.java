package cz.tyckouni.development.server.Managers;


import cz.tyckouni.development.notter.network.Entities.Note;
import cz.tyckouni.development.notter.network.common.IllegalEntityException;
import cz.tyckouni.development.notter.network.common.ServiceFailureException;
import cz.tyckouni.development.notter.network.common.ValidationException;

import java.util.List;

/**
 * Manager class for {@link Note} entities
 */
public interface NoteManager {

    /**
     * Saves {@link Note} data in db and returns generated ID in given Note instance
     *
     * @param note Note to be stored in DB
     * @param notePageId Id of parent notePage
     * @throws IllegalEntityException if note's data are invalid
     * @throws ValidationException if already has ID
     * @throws ServiceFailureException when db operation fails
     */
    void createNote(Note note, Long notePageId) throws IllegalEntityException, ServiceFailureException, ValidationException;

    /**
     * Updates {@link Note} data in DB
     *
     * @param note Note to be updated
     * @throws IllegalEntityException if note's data are invalid or does not have id
     * @throws ValidationException if does not have ID or was not found in DB
     * @throws ServiceFailureException when db operation fails
     */
    void updateNote(Note note) throws IllegalEntityException, ServiceFailureException, ValidationException;

    /**
     * Deletes all {@link Note} data from DB for given NotePageId
     *
     * @param notePageId id to be used for deleting
     * @throws ServiceFailureException when db operation fails
     */
    void deleteNotesByNotePageID(Long notePageId) throws IllegalEntityException, ValidationException, ServiceFailureException;

    /**
     * Finds Note in DB with given ID
     *
     * @param id ID to be used for search
     * @return found Note or null if not found
     * @throws ServiceFailureException when DB operation fails
     */
    Note findNoteById(Long id) throws ServiceFailureException;

    /**
     * Finds all Notes for given {@link cz.tyckouni.development.notter.network.Entities.NoteUser} id id
     *
     * @param id to be used for search
     * @return {@link List} of found Notes
     * @throws ServiceFailureException when DB operation fails
     */
    List<Note> findNotesForNotePageId(Long id) throws ServiceFailureException;
}
