package cz.tyckouni.development.server.Operation;

import cz.tyckouni.development.notter.network.Entities.NotePage;
import cz.tyckouni.development.notter.network.Entities.NoteUser;
import cz.tyckouni.development.notter.network.ServerResponse;
import cz.tyckouni.development.notter.network.common.ServiceFailureException;
import cz.tyckouni.development.server.Managers.NotePageManager;
import cz.tyckouni.development.server.Managers.NotePageManagerImpl;
import cz.tyckouni.development.server.Managers.UserManager;
import cz.tyckouni.development.server.Managers.UserManagerImpl;
import cz.tyckouni.development.server.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Operation class for creating new NoteUser in db
 */
public class CreateNoteUserOperation implements Operation {

    private final static Logger log = LoggerFactory.getLogger(CreateNoteUserOperation.class);

    private Request request;

    private UserManager userManager;
    private NotePageManager notePageManager;

    protected CreateNoteUserOperation(Request request) {
        assert request != null;

        this.request = request;
        this.userManager = new UserManagerImpl(request.getDataSource());
        this.notePageManager = new NotePageManagerImpl(request.getDataSource());
    }

    @Override
    public void call() throws IOException {
        ObjectOutputStream output = request.getOutputStream();
        NoteUser noteUser = request.getNoteUser();

        try {
            userManager.createUser(noteUser);
            notePageManager.createNotePage(new NotePage.Builder()
                    .notes(new ArrayList<>())
                    .width(200)
                    .height(200)
                    .color("#11b0a5")
                    .noteUser(noteUser)
                    .build());
            output.writeObject(ServerResponse.SUCCESS);
            output.writeObject(noteUser);
        } catch (ServiceFailureException e) {
            log.error("Database failure during creating new noteUser", e);
            output.writeObject(ServerResponse.ERROR);
        } finally {
            output.flush();
        }
    }
}
