package cz.tyckouni.development.server.Operation;

import cz.tyckouni.development.notter.network.Entities.NotePage;
import cz.tyckouni.development.notter.network.ServerResponse;
import cz.tyckouni.development.notter.network.common.ServiceFailureException;
import cz.tyckouni.development.server.Managers.NotePageManager;
import cz.tyckouni.development.server.Managers.NotePageManagerImpl;
import cz.tyckouni.development.server.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Operation class for removing NotePage from DB
 */
public class RemoveNotePageOperation implements Operation {

    private final static Logger log = LoggerFactory.getLogger(RemoveNotePageOperation.class);

    private Request request;

    private NotePageManager notePageManager;

    protected RemoveNotePageOperation(Request request) {
        assert request != null;

        this.notePageManager = new NotePageManagerImpl(request.getDataSource());
        this.request = request;
    }

    @Override
    public void call() throws IOException {
        ObjectInputStream input = request.getInputStream();
        ObjectOutputStream output = request.getOutputStream();

        try {
            NotePage notePage = null;
            try {
                notePage = (NotePage) input.readObject();
                NotePage dbNotePage = notePageManager.findNotePageById(notePage.getId());
                notePageManager.deleteNotePage(dbNotePage);
                output.writeObject(ServerResponse.SUCCESS);
            } catch (ClassNotFoundException e) {
                log.error("Invalid data arrived", e);
                output.writeObject(ServerResponse.ERROR);
                throw new RuntimeException("Invalid data arrived", e);
            } catch (ServiceFailureException e) {
                log.error("Database failure during removing NotePage data: " + notePage, e);
                output.writeObject(ServerResponse.ERROR);
            }
        } finally {
            output.flush();
        }
    }
}
