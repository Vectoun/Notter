package cz.tyckouni.development.server.Operation;

import cz.tyckouni.development.notter.network.Entities.NotePage;
import cz.tyckouni.development.notter.network.ServerResponse;
import cz.tyckouni.development.notter.network.common.ServiceFailureException;
import cz.tyckouni.development.server.Managers.NotePageManager;
import cz.tyckouni.development.server.Managers.NotePageManagerImpl;
import cz.tyckouni.development.server.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Operation for updating NotePage in DB
 */
public class UpdateNotePageOperation implements Operation {

    private final static Logger log = LoggerFactory.getLogger(UpdateNotePageOperation.class);

    private Request request;

    private NotePageManager notePageManager;

    protected UpdateNotePageOperation(Request request) {
        assert request != null;

        this.request = request;
        this.notePageManager = new NotePageManagerImpl(request.getDataSource());
    }

    @Override
    public void call() throws IOException {
        ObjectOutputStream output = request.getOutputStream();
        ObjectInputStream input = request.getInputStream();

        try {
            NotePage notePage = (NotePage) input.readObject();
            notePageManager.updateNotePage(notePage);
            output.writeObject(ServerResponse.SUCCESS);
            output.writeObject(notePage);
        } catch (ServiceFailureException e) {
            log.error("Database failure during creating new note", e);
            output.writeObject(ServerResponse.ERROR);
        } catch (ClassNotFoundException e) {
            log.error("Invalid data arrived ", e);
            throw new RuntimeException("Invalid data arrived ", e);
        } finally {
            output.flush();
        }
    }
}
