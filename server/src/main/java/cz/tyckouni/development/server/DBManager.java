package cz.tyckouni.development.server;

import com.mysql.cj.jdbc.MysqlDataSource;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.derby.jdbc.EmbeddedDriver;
import org.postgresql.ds.PGPoolingDataSource;
import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

/**
 * DBManager class for db settings operations
 */
public class DBManager {

    private static BasicDataSource database;

    private static MysqlDataSource mysqlDS = null;

    private static PGSimpleDataSource pgDB;

    public synchronized static DataSource getDatabaseDataSource() {
        if(database == null) {
            database = new BasicDataSource();
            database.setDriverClassName(EmbeddedDriver.class.getName());
            database.setUrl("jdbc:derby:memory:translatorAgencyDB;create=true");

            new ResourceDatabasePopulator(
                    new ClassPathResource("test-database.sql"),
                    new ClassPathResource("test-data.sql"))
                    .execute(database);
        }
        return database;
    }

    public synchronized static DataSource getMySQLDataSource() {
        if (mysqlDS != null) {
            return mysqlDS;
        }
        Properties props = new Properties();
        try {
            props.load(DBManager.class.getResourceAsStream("/db.properties"));
            mysqlDS = new MysqlDataSource();
            mysqlDS.setURL(props.getProperty("MYSQL_DB_URL"));
            mysqlDS.setUser(props.getProperty("MYSQL_DB_USERNAME"));
            mysqlDS.setPassword(props.getProperty("MYSQL_DB_PASSWORD"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mysqlDS;
    }

    public synchronized static DataSource getPGDataSource() {
        if (pgDB != null) {
            return pgDB;
        }
        Properties props = new Properties();
        try {
            props.load(DBManager.class.getResourceAsStream("/db.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        pgDB = new PGSimpleDataSource();
        pgDB.setUrl(props.getProperty("MYSQL_DB_URL"));
        pgDB.setUser(props.getProperty("MYSQL_DB_USERNAME"));
        pgDB.setPassword(props.getProperty("MYSQL_DB_PASSWORD"));

        return pgDB;
    }
}
