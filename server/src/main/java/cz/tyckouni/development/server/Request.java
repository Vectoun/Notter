package cz.tyckouni.development.server;

import cz.tyckouni.development.notter.network.Entities.NoteUser;

import javax.sql.DataSource;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Entity class representing client request
 */
public final class Request {
    private ObjectOutputStream outputStream;
    private ObjectInputStream inputStream;
    private NoteUser noteUser;
    private DataSource dataSource;

    protected Request(ObjectOutputStream outputStream,
                   ObjectInputStream inputStream,
                   NoteUser noteUser,
                   DataSource dataSource) {

        assert dataSource != null;
        assert outputStream != null;
        assert inputStream != null;

        this.outputStream = outputStream;
        this.inputStream = inputStream;
        this.noteUser = noteUser;
        this.dataSource = dataSource;
    }

    public ObjectOutputStream getOutputStream() {
        return outputStream;
    }

    public ObjectInputStream getInputStream() {
        return inputStream;
    }

    public NoteUser getNoteUser() {
        return noteUser;
    }

    public DataSource getDataSource() {
        return dataSource;
    }
}
