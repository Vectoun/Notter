package cz.tyckouni.development.server.Managers;


import cz.tyckouni.development.notter.network.Entities.Note;
import cz.tyckouni.development.notter.network.Entities.NotePage;
import cz.tyckouni.development.notter.network.Entities.NoteUser;
import cz.tyckouni.development.notter.network.common.IllegalEntityException;
import cz.tyckouni.development.notter.network.common.ServiceFailureException;
import cz.tyckouni.development.notter.network.common.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;
import java.util.List;

/**
 * Implementation class for {@link NotePageManager}
 */
public class NotePageManagerImpl implements NotePageManager {

    private final static Logger log = LoggerFactory.getLogger(NotePageManagerImpl.class);

    private JdbcTemplate jdbcTemplate;
    private UserManager userManager;
    private NoteManager noteManager;

    public NotePageManagerImpl(DataSource dataSource) {
        if (dataSource == null) {
            throw new IllegalArgumentException("Argument datasource is null.");
        }
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.userManager = new UserManagerImpl(dataSource);
        this.noteManager = new NoteManagerImpl(dataSource);
    }

    private void checkNotePageState(NotePage notePage) throws IllegalEntityException {
        if(notePage.getHeight() < 0) {
            throw new IllegalEntityException("Height has negative value");
        }
        if(notePage.getWidth() < 0) {
            throw new IllegalEntityException("Width has negative value");
        }
        if(!notePage.getColor().matches("#[a-f0-9][a-f0-9][a-f0-9][a-f0-9][a-f0-9][a-f0-9]")) {
            throw new IllegalEntityException("Color has wrong format: " + notePage.getColor());
        }
        if(notePage.getNoteUser() == null) {
            throw new IllegalEntityException("User is null");
        }
        if(notePage.getNoteUser().getId() == null) {
            throw new IllegalEntityException("User's id is null");
        }
    }

    private void createNotes(List<Note> notes, Long notePageId) throws ServiceFailureException {
        if(notes == null) {
            throw new IllegalArgumentException("Notes are null");
        }
        notes.forEach(note -> note.setId(null));
        for (Note note : notes) {
            noteManager.createNote(note, notePageId);
        }
    }

    @Override
    public void createNotePage(NotePage notePage) throws IllegalEntityException, ValidationException, ServiceFailureException {
        if(notePage == null) {
            throw new IllegalArgumentException("NotePage is null.");
        }
        if(notePage.getId() != null) {
            throw new ValidationException("NotePage has signed ID: " + notePage.getId());
        }
        if(notePage.getWidth() < 100) {
            log.error("NotePage has invalid width: " + notePage.getWidth());
            throw new IllegalEntityException("NotePage has invalid width: " + notePage.getWidth());
        }
        if(notePage.getHeight() < 100) {
            log.error("NotePage has invalid height: " + notePage.getWidth());
            throw new IllegalEntityException("NotePage has invalid height: " + notePage.getHeight());
        }
        checkNotePageState(notePage);

        if(notePage.getId() != null) {
            throw new ValidationException("NotePage has already signed id");
        }
        try {
            SimpleJdbcInsert insertNotePage = new SimpleJdbcInsert(jdbcTemplate)
                    .withTableName("notePage").usingGeneratedKeyColumns("id");

            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("color", notePage.getColor())
                    .addValue("noteUserId", notePage.getNoteUser().getId())
                    .addValue("height", notePage.getHeight())
                    .addValue("width", notePage.getWidth());

            Number id = insertNotePage.executeAndReturnKey(parameters);
            notePage.setId(id.longValue());

            createNotes(notePage.getNotes(), notePage.getId());
        } catch (DataAccessException e) {
            throw new ServiceFailureException("Database failure during creating NotePage", e);
        }
    }

    @Override
    public void updateNotePage(NotePage notePage) throws IllegalEntityException, ValidationException, ServiceFailureException {
        if(notePage == null) {
            log.error("NotePage is null");
            throw new IllegalArgumentException("NotePage is null");
        }
        if(notePage.getId() == null) {
            log.error("NotePage has null id");
            throw new ValidationException("NotePage has null id");
        }
        if(notePage.getWidth() < 100) {
            log.error("NotePage has invalid width: " + notePage.getWidth());
            throw new IllegalEntityException("NotePage has invalid width: " + notePage.getWidth());
        }
        if(notePage.getHeight() < 100) {
            log.error("NotePage has invalid height: " + notePage.getWidth());
            throw new IllegalEntityException("NotePage has invalid height: " + notePage.getHeight());
        }
        checkNotePageState(notePage);

        if(findNotePageById(notePage.getId()) == null) {
            throw new ValidationException("NotePage is not in DB");
        }
        try {
            jdbcTemplate.update("UPDATE notePage SET noteUserId=?, color=?, width=?,height=? WHERE id=?",
                    notePage.getNoteUser().getId(),
                    notePage.getColor(),
                    notePage.getWidth(),
                    notePage.getHeight(),
                    notePage.getId());

            noteManager.deleteNotesByNotePageID(notePage.getId());
            createNotes(notePage.getNotes(), notePage.getId());
        } catch (DataAccessException e) {
            log.error("Failed to access db during updating notePage");
            throw new ServiceFailureException("Failed to access db during updating notePage");
        }
    }

    @Override
    public void deleteNotePage(NotePage notePage) throws ValidationException, ServiceFailureException {
        if(notePage == null) {
            throw new IllegalArgumentException("NotePage is null.");
        }
        if(notePage.getId() == null) {
            throw new ValidationException("Id can not be null.");
        }
        if(findNotePageById(notePage.getId()) == null) {
            throw new ValidationException("NotePage is not in DB.");
        }
        try {
            jdbcTemplate.update("DELETE FROM notePage WHERE id=?", notePage.getId());
        } catch (DataAccessException e) {
            throw new ServiceFailureException("Failed to access database during deleting notePage.");
        }
    }


    private RowMapper<NotePage> notePageRowMapper = (rs, num) ->
        new NotePage.Builder()
            .id(rs.getLong("id"))
            .noteUser(userManager.findUserById(rs.getLong("noteUserId")))
            .notes(noteManager.findNotesForNotePageId(rs.getLong("id")))
            .width(rs.getInt("width"))
            .height(rs.getInt("height"))
            .color(rs.getString("color"))
            .build();


    @Override
    public NotePage findNotePageById(Long id) throws ServiceFailureException {
        if(id == null) {
            throw new IllegalArgumentException("Id is null.");
        }
        try {
            return jdbcTemplate.queryForObject("SELECT * FROM notePage WHERE id=?", notePageRowMapper, id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        } catch (DataAccessException e) {
            throw new ServiceFailureException("Database failure during getting notePage.", e);
        }
    }

    @Override
    public List<NotePage> getNotePagesForUser(NoteUser noteUser) throws ServiceFailureException {
        if(noteUser == null) {
            throw new IllegalArgumentException("NotePage is null.");
        }
        if(noteUser.getId() == null) {
            throw new ValidationException("NoteUser has null id.");
        }
        if(userManager.findUserById(noteUser.getId()) == null) {
            throw new ValidationException("Given noteUser does not exist.");
        }
        try {
            return jdbcTemplate.query("SELECT * FROM notePage WHERE noteUserId=?", notePageRowMapper, noteUser.getId());
        } catch (DataAccessException e) {
            throw new ServiceFailureException("Failed to access DB during getting notePages for noteUser.");
        }
    }

    @Override
    public List<NotePage> getAllNotePages() throws ServiceFailureException {
        try {
            return jdbcTemplate.query("SELECT * FROM notePage ", notePageRowMapper);
        } catch (DataAccessException e) {
            throw new ServiceFailureException("Failed to access DB during getting sll notePages.");
        }
    }
}
