package cz.tyckouni.development.server.Managers;


import cz.tyckouni.development.notter.network.Entities.NoteUser;
import cz.tyckouni.development.notter.network.common.IllegalEntityException;
import cz.tyckouni.development.notter.network.common.ServiceFailureException;
import cz.tyckouni.development.notter.network.common.ValidationException;

import java.util.List;

/**
 * Interface for {@link NoteUser} manager
 * Provides operations for managing {@link NoteUser} entities
 */
public interface UserManager {
    /**
     * Saves {@link NoteUser} data into db and returns generated id in given instance
     *
     * @param noteUser {@link NoteUser} to be saved
     * @throws IllegalArgumentException when user is null
     * @throws IllegalEntityException if user has invalid attributes
     * @throws ValidationException if notePage has already signed id, email or user name is already used
     * @throws ServiceFailureException when db operation fails
     */
    void createUser(NoteUser noteUser) throws IllegalEntityException, ValidationException, ServiceFailureException;

    /**
     * Edits {@link NoteUser} data in db
     *
     * @param noteUser {@link NoteUser} to be edited
     * @throws IllegalArgumentException when user is null
     * @throws IllegalEntityException if user has invalid attributes
     * @throws ValidationException if noteUser has null id or does not exist in db,, email or noteUser name is already used
     * @throws ServiceFailureException when db operation fails
     */
    void updateUser(NoteUser noteUser) throws IllegalEntityException, ValidationException, ServiceFailureException;

    /**
     * Deletes user data from db.
     *
     * @param noteUser {@link NoteUser} to be deleted from db
     * @throws IllegalArgumentException when user is null
     * @throws ValidationException when given user has null id or does not exist in db
     * @throws ServiceFailureException when db operation fails
     */
    void deleteUser(NoteUser noteUser) throws IllegalEntityException, ValidationException, ServiceFailureException;

    /**
     * Finds user in db by given id
     *
     * @param id to be used for searching
     * @return {@link NoteUser} or null if not found
     * @throws IllegalArgumentException if given id is null
     * @throws ServiceFailureException when db operation fails
     * @throws ValidationException if user does not exist in db
     */
    NoteUser findUserById(Long id) throws ServiceFailureException, ValidationException;

    /**
     * Finds all users in database
     *
     * @throws ServiceFailureException when db operation fails
     * @return {@link List} of {@link NoteUser}
     */
    List<NoteUser> getAllUsers() throws ServiceFailureException;

    /**
     * Finds noteuser by given name or null if not found
     *
     * @return {@link NoteUser} if found, null otherwise
     * @param name name to be used for search
     * @throws ServiceFailureException when DB operation fails
     */
    NoteUser findNoteUserByUserName(String name) throws ServiceFailureException;
}
