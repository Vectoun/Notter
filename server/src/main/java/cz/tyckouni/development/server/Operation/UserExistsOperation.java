package cz.tyckouni.development.server.Operation;

import cz.tyckouni.development.notter.network.Entities.NoteUser;
import cz.tyckouni.development.notter.network.ServerResponse;
import cz.tyckouni.development.notter.network.common.ServiceFailureException;
import cz.tyckouni.development.server.Managers.UserManager;
import cz.tyckouni.development.server.Managers.UserManagerImpl;
import cz.tyckouni.development.server.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Created by Vectoun on 13. 9. 2017.
 */
public class UserExistsOperation implements Operation {

    private final static Logger log = LoggerFactory.getLogger(VerifyUserOperation.class);

    private Request request;

    private UserManager userManager;

    protected UserExistsOperation(Request request) {
        assert request != null;

        this.request = request;
        this.userManager = new UserManagerImpl(request.getDataSource());
    }

    @Override
    public void call() throws IOException {
        NoteUser noteUser = request.getNoteUser();
        ObjectOutputStream output = request.getOutputStream();

        try {
            NoteUser existingUser = userManager.findNoteUserByUserName(noteUser.getUserName());
            if (existingUser != null &&
                    noteUser.getUserName().equals(existingUser.getUserName())) {
                output.writeObject(ServerResponse.SUCCESS);
            } else {
                output.writeObject(ServerResponse.FAILURE);
            }
        } catch (ServiceFailureException e) {
            log.error("Database failure during verifying noteUser", e);
            output.writeObject(ServerResponse.ERROR);
            //TODO:
        } finally {
            output.flush();
        }
    }
}
