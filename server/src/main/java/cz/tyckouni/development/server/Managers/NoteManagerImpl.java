package cz.tyckouni.development.server.Managers;

import cz.tyckouni.development.notter.network.Entities.Note;
import cz.tyckouni.development.notter.network.common.IllegalEntityException;
import cz.tyckouni.development.notter.network.common.ServiceFailureException;
import cz.tyckouni.development.notter.network.common.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * NoteManager implementation class
 */
public class NoteManagerImpl implements NoteManager {

    final static Logger log = LoggerFactory.getLogger(NoteManagerImpl.class);

    private JdbcTemplate jdbcTemplate;

    public NoteManagerImpl(DataSource dataSource) {
        if(dataSource == null) {
            throw new IllegalArgumentException("Datasource is null");
        }
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void createNote(Note note, Long notePageId) throws IllegalEntityException, ServiceFailureException, ValidationException {
        if(note == null) {
            throw new IllegalArgumentException("note is null");
        }
        if(note.getId() != null) {
            throw new ValidationException("Note has signed id");
        }
        try {
            SimpleJdbcInsert insertNote = new SimpleJdbcInsert(jdbcTemplate)
                    .withTableName("note").usingGeneratedKeyColumns("id");

            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("notePageId", notePageId)
                    .addValue("data", note.getData());

            Number id = insertNote.executeAndReturnKey(parameters);
            note.setId(id.longValue());
        } catch (DataAccessException e) {
            log.error("DB failure during creating note " + note, e);
            throw new ServiceFailureException("DB failure during creating note " + note, e);
        }
    }

    @Override
    public void updateNote(Note note) throws IllegalEntityException, ServiceFailureException, ValidationException {
        if(note == null) {
            throw new IllegalArgumentException("Note is null");
        }
        if(note.getId() == null) {
            throw new ValidationException("Note's id is null");
        }
        if(findNoteById(note.getId()) == null) {
            throw new ValidationException("Note is not in DB");
        }
        try {
            jdbcTemplate.update("UPDATE note SET data=? WHERE id=?",
                    note.getData(), note.getId());
        } catch (DataAccessException e) {
            log.error("Failed to access database during updating note: " + note, e);
            throw new ServiceFailureException("Failed to access database during updating note: " + note, e);
        }
    }

    @Override
    public void deleteNotesByNotePageID(Long notePageId) throws IllegalEntityException, ValidationException, ServiceFailureException {
        if(notePageId == null) {
            throw new IllegalArgumentException("Note is null");
        }
        try {
            jdbcTemplate.update("DELETE FROM note WHERE notePageId=?", notePageId);
        } catch (DataAccessException e) {
            log.error("Failed to access DB during deleting notes with notePageID: " + notePageId, e);
            throw new ServiceFailureException("Failed to access DB during deleting notes with notePageID: " + notePageId, e);
        }
    }

    private RowMapper<Note> noteRowMapper = (ResultSet rs, int rowNum) ->
            new Note(rs.getLong("id"),
                    rs.getString("data"));

    @Override
    public Note findNoteById(Long id) throws ServiceFailureException {
        if(id == null) {
            throw new IllegalArgumentException("Id is null.");
        }
        try {
            return jdbcTemplate.queryForObject("SELECT id, data FROM note WHERE id=?", noteRowMapper, id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        } catch (DataAccessException e) {
            log.error("Failed to access database while trying to find note by id: " + id);
            throw new ServiceFailureException("Failed to access database while trying to find note by id: " + id);
        }
    }

    @Override
    public List<Note> findNotesForNotePageId(Long id) throws ServiceFailureException {
        if(id == null) {
            throw new IllegalArgumentException("Id is null");
        }
        try {
            return jdbcTemplate.query("SELECT id, data FROM note WHERE notePageId=?", noteRowMapper, id);
        } catch (EmptyResultDataAccessException e) {
            return new ArrayList<>();
        } catch (DataAccessException e) {
            log.error("Database failure during trying to get all notes for notePage with id: " + id);
            throw new ServiceFailureException(
                    "Database failure during trying to get all notes for notePage with id: " + id);
        }
    }
}
