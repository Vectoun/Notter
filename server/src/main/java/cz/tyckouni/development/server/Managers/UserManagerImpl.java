package cz.tyckouni.development.server.Managers;

import cz.tyckouni.development.notter.network.Entities.NoteUser;
import cz.tyckouni.development.notter.network.common.DuplicateEmailException;
import cz.tyckouni.development.notter.network.common.DuplicateUserNameEception;
import cz.tyckouni.development.notter.network.common.IllegalEntityException;
import cz.tyckouni.development.notter.network.common.ServiceFailureException;
import cz.tyckouni.development.notter.network.common.ValidationException;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.List;

/**
 * Implementation class for {@link UserManager}
 */
public class UserManagerImpl implements UserManager {

    private JdbcTemplate jdbcTemplate;

    public UserManagerImpl(DataSource dataSource) {
        if(dataSource == null) {
            throw new IllegalArgumentException("Argument 'Datasource' is null");
        }
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void createUser(NoteUser noteUser) throws IllegalEntityException, ValidationException, ServiceFailureException {
        if (noteUser == null) {
            throw new IllegalArgumentException("NoteUser is null");
        }
        if(noteUser.getUserName() == null) {
            throw new IllegalEntityException("NoteUser's name is null");
        }
        if(noteUser.getEmailAddress() == null) {
            throw new IllegalEntityException("NoteUser's email is null");
        }
        if(noteUser.getPasswordHash() == null) {
            throw new IllegalEntityException("NoteUser's passwordHash is null");
        }
        if(noteUser.getId() != null) {
            throw new ValidationException("Id has to be null");
        }
        if(isEmailAlreadyUsed(noteUser.getEmailAddress())) {
            throw new DuplicateEmailException("Email already used");
        }
        if(isNameAlreadyUsed(noteUser.getUserName())) {
            throw new DuplicateUserNameEception("NoteUser name already used");
        }
        try {
            SimpleJdbcInsert insertUser = new SimpleJdbcInsert(jdbcTemplate)
                    .withTableName("noteUser").usingGeneratedKeyColumns("id");

            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("userName", noteUser.getUserName())
                    .addValue("email", noteUser.getEmailAddress())
                    .addValue("passwordHash", noteUser.getPasswordHash());

            Number id = insertUser.executeAndReturnKey(parameters);
            noteUser.setId(id.longValue());
        } catch (DataAccessException e) {
            throw new ServiceFailureException("Failed to access db during creating noteUser", e);
        }
    }

    @Override
    public void updateUser(NoteUser noteUser) throws IllegalEntityException, ServiceFailureException, ValidationException {
        if (noteUser == null) {
            throw new IllegalArgumentException("NoteUser is null");
        }
        if(noteUser.getUserName() == null) {
            throw new IllegalEntityException("NoteUser's name is null");
        }
        if(noteUser.getEmailAddress() == null) {
            throw new IllegalEntityException("NoteUser's email is null");
        }
        if(noteUser.getPasswordHash() == null) {
            throw new IllegalEntityException("NoteUser's passwordHash is null");
        }
        if(noteUser.getId() == null) {
            throw new ValidationException("Id can not be null.");
        }

        NoteUser oldUser = findUserById(noteUser.getId());
        if(oldUser == null) {
            throw new ValidationException("No noteUser with given id was found");
        }
        if(!oldUser.getUserName().equals(noteUser.getUserName())) {
            if(isNameAlreadyUsed(noteUser.getUserName())) {
                throw new DuplicateUserNameEception("NoteUser name is already used.");
            }
        }
        if(!oldUser.getEmailAddress().equals(noteUser.getEmailAddress())) {
            if(isEmailAlreadyUsed(noteUser.getEmailAddress())) {
                throw new DuplicateEmailException("Email is already used.");
            }
        }

        try {
            jdbcTemplate.update("UPDATE noteUser SET userName=?, email=?, passwordHash=? WHERE id=?",
                    noteUser.getUserName(), noteUser.getEmailAddress(), noteUser.getPasswordHash(), noteUser.getId());
        } catch (DataAccessException e) {
            throw new ServiceFailureException("Failed to access database during updating noteUser", e);
        }
    }

    @Override
    public void deleteUser(NoteUser noteUser) throws ValidationException, ServiceFailureException, IllegalEntityException {
        if (noteUser == null) {
            throw new IllegalArgumentException("NoteUser is null");
        }
        if(noteUser.getId() == null) {
            throw new ValidationException("Id can not be null.");
        }

        try {
            int rowsAffected = jdbcTemplate.update("DELETE FROM noteUser WHERE id=?", noteUser.getId());
            if(rowsAffected != 1) {
                throw new ValidationException("No noteUser with given id was found");
            }
        } catch (DataAccessException e) {
            throw new ServiceFailureException("Failed to access database during deleting noteUser", e);
        }
    }

    private RowMapper<NoteUser> userRowMapper = (ResultSet rs, int rowNum) ->
            new NoteUser.Builder()
            .id(rs.getLong("id"))
            .userName(rs.getString("userName"))
            .emailAddress(rs.getString("email"))
            .passwordHash(rs.getString("passwordHash"))
            .build();

    @Override
    public NoteUser findUserById(Long id) throws ServiceFailureException {
        if (id == null) {
            throw new IllegalArgumentException("Id is null");
        }
        try {
            return jdbcTemplate.queryForObject("SELECT * FROM noteUser WHERE id=?", userRowMapper, id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        } catch (DataAccessException e) {
            throw new ServiceFailureException("Failed to access database while trying to find noteUser by id.", e);
        }
    }

    @Override
    public List<NoteUser> getAllUsers() throws ServiceFailureException {
        try {
            return jdbcTemplate.query("SELECT * FROM noteUser", userRowMapper);
        } catch (DataAccessException e) {
            throw new ServiceFailureException("Failed to access db while getting all noteUsers", e);
        }
    }

    private boolean isNameAlreadyUsed(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name is null");
        }
        try {
            jdbcTemplate.queryForObject("SELECT * FROM noteUser WHERE userName=?", userRowMapper, name);
        }
        catch(EmptyResultDataAccessException e) {
            return false;
        }
        return true;
    }

    private boolean isEmailAlreadyUsed(String email) throws ServiceFailureException {
        if (email == null) {
            throw new IllegalArgumentException("Email is null");
        }
        try {
            jdbcTemplate.queryForObject("SELECT * FROM noteUser WHERE email=?", userRowMapper, email);
        }
        catch(EmptyResultDataAccessException e) {
            return false;
        }
        return true;
    }

    @Override
    public NoteUser findNoteUserByUserName(String name) throws ServiceFailureException {
        if(name == null) {
            throw new IllegalArgumentException("name is null");
        }
        try {
            return jdbcTemplate.queryForObject("SELECT * FROM noteUser WHERE userName=?", userRowMapper, name);
        } catch (EmptyResultDataAccessException e) {
            return null;
        } catch (DataAccessException e) {
            throw new ServiceFailureException("Failed to access database while trying to find noteUser by name.", e);
        }
    }
}
