package cz.tyckouni.development.server.Managers;

import cz.tyckouni.development.notter.network.Entities.NotePage;
import cz.tyckouni.development.notter.network.Entities.NoteUser;
import cz.tyckouni.development.notter.network.common.IllegalEntityException;
import cz.tyckouni.development.notter.network.common.ServiceFailureException;
import cz.tyckouni.development.notter.network.common.ValidationException;

import java.util.List;

/**
 * Interface for {@link NotePage} manager
 * Provides method for operations with {@link NotePage} entities
 */
public interface NotePageManager {

    /**
     * Saves NotePage data to database and returns generated id in given instance
     *
     * @param notePage note page to be saved to database
     * @throws IllegalArgumentException if notePage is null
     * @throws IllegalEntityException if notePage has invalid attributes, width, height are not in range,
     * color has wrong format
     * @throws ValidationException if notePage has already signed id
     * @throws ServiceFailureException when db operation fails
     */
    void createNotePage(NotePage notePage) throws ServiceFailureException, IllegalEntityException, ValidationException;

    /**
     * Updates NotePage data in database
     *
     * @param notePage notePage to be updated
     * @throws IllegalArgumentException if notePage is null
     * @throws IllegalEntityException if notePage has invalid attributes, width, height are not in range
     *  or color has wrong format
     * @throws ValidationException if notePage does not have id or does not exists in db
     * @throws ServiceFailureException when db operation fails
     */
    void updateNotePage(NotePage notePage) throws ServiceFailureException, IllegalEntityException, ValidationException;

    /**
     * Deletes NotePage data from database
     *
     * @param notePage notePage to be deleted
     * @throws IllegalArgumentException if notePage is null
     * @throws ValidationException if notePage does not have id or does not exists in db
     * @throws ServiceFailureException when db operation fails
     */
    void deleteNotePage(NotePage notePage) throws ServiceFailureException, ValidationException;

    /**
     * Finds notePage in db by given id
     *
     * @param id id to be used for searching
     * @throws IllegalArgumentException if id is null
     * @throws ServiceFailureException when db operation fails
     * @return notePage
     */
    NotePage findNotePageById(Long id) throws ServiceFailureException, ValidationException;

    /**
     * Finds all notePages for given noteUser and returns them as a List
     *
     * @param noteUser noteUser that will be used for searching
     * @throws IllegalArgumentException if noteUser is null
     * @throws ValidationException if given noteUser has null id or does not exists in db
     * @throws ServiceFailureException when db operation fails
     * @return {@link List} of {@link NotePage} for given {@link NoteUser}
     */
    List<NotePage> getNotePagesForUser(NoteUser noteUser) throws ServiceFailureException, ValidationException;

    /**
     * Finds all notePages
     *
     * @return {@link List} of all {@link NotePage} in db
     * @throws ServiceFailureException when db operation fails
     */
    List<NotePage> getAllNotePages() throws ServiceFailureException;
}
