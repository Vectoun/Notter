package cz.tyckouni.development.server;

import cz.tyckouni.development.notter.network.Entities.NoteUser;
import cz.tyckouni.development.notter.network.ServerAction;

import cz.tyckouni.development.server.Operation.Operation;
import cz.tyckouni.development.server.Operation.OperationFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Class for handling connection
 */
public class ConnectionHandler implements Runnable {

    private DataSource dataSource;

    private final static Logger log = LoggerFactory.getLogger(ConnectionHandler.class);
    private Socket socket;

    ConnectionHandler(Socket socket, DataSource dataSource) {
        assert socket != null;
        assert dataSource != null;

        this.socket = socket;
        this.dataSource = dataSource;
    }

    @Override
    public void run() {

        try {
            ObjectOutputStream objectOutput = new ObjectOutputStream((socket.getOutputStream()));
            ObjectInputStream objectInput = new ObjectInputStream((socket.getInputStream()));
            ServerAction action;

            do {
                if(socket.isClosed()) {
                    break;
                }

                action = (ServerAction) objectInput.readObject();

                if (action == ServerAction.CLOSE) {
                    break;
                }

                NoteUser noteUser = (NoteUser) objectInput.readObject();
                log.info("Client " + noteUser.getId() + " connected");

                Request request = new Request(objectOutput, objectInput, noteUser, dataSource);
                Operation operation = OperationFactory.fromRequestAndAction(request, action);

                operation.call();

            } while (!socket.isClosed()) ;
        } catch(IOException e){
            log.error("An exception occurred during communication with noteUser", e);
        } catch(ClassNotFoundException e){
            log.error("Received invalid data", e);
        } catch (Exception e) {
            log.error("Unknown exception happened " + e.getMessage());
        } finally {
            if (socket != null && !socket.isClosed()) {
                try {
                    socket.close();
                    log.info("Server socket was successfully closed");
                } catch (IOException e) {
                    log.error("Failed to close server socket", e);
                }
            }
        }
    }
}
