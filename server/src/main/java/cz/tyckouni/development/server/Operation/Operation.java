package cz.tyckouni.development.server.Operation;

import java.io.IOException;

/**
 * Interface for server operations for client
 */
public interface Operation {

    void call() throws IOException;
}
