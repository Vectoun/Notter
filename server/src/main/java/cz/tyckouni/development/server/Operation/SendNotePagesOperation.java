package cz.tyckouni.development.server.Operation;

import cz.tyckouni.development.notter.network.Entities.NotePage;
import cz.tyckouni.development.notter.network.Entities.NoteUser;
import cz.tyckouni.development.notter.network.ServerResponse;
import cz.tyckouni.development.notter.network.common.ServiceFailureException;
import cz.tyckouni.development.server.Managers.NotePageManager;
import cz.tyckouni.development.server.Managers.NotePageManagerImpl;
import cz.tyckouni.development.server.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

/**
 * Operation for sending List of NotePages to user
 */
public class SendNotePagesOperation implements Operation{

    private final static Logger log = LoggerFactory.getLogger(SendNotePagesOperation.class);

    private Request request;

    private NotePageManager notePageManager;

    protected SendNotePagesOperation(Request request) {
        assert request != null;

        this.request = request;
        this.notePageManager = new NotePageManagerImpl(request.getDataSource());
    }

    @Override
    public void call() throws IOException {
        ObjectOutputStream output = request.getOutputStream();
        NoteUser noteUser = request.getNoteUser();

        try {
            List<NotePage> notePages = notePageManager.getNotePagesForUser(noteUser);
            output.writeObject(ServerResponse.SUCCESS);
            output.writeObject(notePages);
        } catch (ServiceFailureException e) {
            log.error("Database failure during sending notePages to noteUser", e);
            output.writeObject(ServerResponse.ERROR);
        } finally {
            output.flush();
        }
    }
}
