CREATE TABLE noteUser (
  id           SERIAL PRIMARY KEY,
  userName     VARCHAR(70),
  email        VARCHAR(100),
  passwordHash VARCHAR(100)
);

CREATE TABLE notePage (
  id                           SERIAL PRIMARY KEY,
  noteUserId                       INT REFERENCES noteUser (id)
    ON DELETE CASCADE,
  color                        VARCHAR(7),
  width                        INT,
  height                       INT
);

CREATE TABLE note (
  id         SERIAL PRIMARY KEY,
  notePageId INT REFERENCES notePage (id)
    ON DELETE CASCADE,
  data       VARCHAR(100)
);