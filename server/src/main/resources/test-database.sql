CREATE TABLE noteUser (
  id           INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  userName     VARCHAR(70),
  email        VARCHAR(100),
  passwordHash VARCHAR(100)
);

CREATE TABLE notePage (
  id                           INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  noteUserId                       INT REFERENCES noteUser (id)
    ON DELETE CASCADE,
  color                        VARCHAR(7),
  width                        INT,
  height                       INT
);

CREATE TABLE note (
  id         INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  notePageId INT REFERENCES notePage (id)
    ON DELETE CASCADE,
  data       VARCHAR(100)
);