import cz.tyckouni.development.notter.network.Entities.Note;
import cz.tyckouni.development.notter.network.Entities.NotePage;
import cz.tyckouni.development.notter.network.Entities.NoteUser;
import cz.tyckouni.development.notter.network.common.IllegalEntityException;
import cz.tyckouni.development.notter.network.common.ValidationException;
import cz.tyckouni.development.server.Managers.NotePageManagerImpl;
import cz.tyckouni.development.server.Managers.UserManagerImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

/**
 * Test class for {@link NotePageManagerImpl}
 */
public class NotePageManagerImplTest {
    private NotePageManagerImpl notePageManager;
    private UserManagerImpl userManager;
    private EmbeddedDatabase db;

    private NoteUser mike = new NoteUser.Builder()
            .emailAddress("mike@gmail.com")
                .passwordHash("dfdfdf5656d")
                .userName("mike")
                .build();
    private NoteUser suzan = new NoteUser.Builder()
            .emailAddress("suzan@gmail.com")
            .passwordHash("dfddf52546d")
            .userName("suzan")
            .build();
    private NoteUser pete = new NoteUser.Builder()
            .emailAddress("pete@gmail.com")
            .passwordHash("dfdfdf5656d")
            .userName("pete")
            .build();

    private NotePage mikeNotePage() throws Exception {
        return new NotePage.Builder()
                .height(100)
                .width(100)
                .color("#512035")
                .notes(Arrays.asList(new Note(null, "R9sr"), new Note(null, "testset")))
                .noteUser(mike)
                .build();
    }

    private NotePage peteNotePage() throws  Exception {
        return new NotePage.Builder()
                .height(100)
                .width(150)
                .color("#512037")
                .notes(Arrays.asList(new Note(null, "DFdfdr"), new Note(null, "tdfdfdfdfet")))
                .noteUser(pete)
                .build();
    }

    private NotePage suzanNotePage() throws Exception {
        return new NotePage.Builder()
                .height(250)
                .width(250)
                .color("#512036")
                .notes(Arrays.asList(new Note(null, "aaaaafdr"), new Note(null, "bbbbbbf")))
                .noteUser(suzan)
                .build();
    }

    @Before
    public void setUp() throws Exception {
        db = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.DERBY)
                .addScript("test-database.sql")
                .build();

        notePageManager = new NotePageManagerImpl(db);
        userManager = new UserManagerImpl(db);

        userManager.createUser(mike);
        userManager.createUser(pete);
        userManager.createUser(suzan);
    }

    @After
    public void tearDown() throws Exception {
        db.shutdown();
    }

    @Test
    public void createNotePageTest() throws Exception {
        NotePage mikes = mikeNotePage();
        NotePage suzans = suzanNotePage();

        notePageManager.createNotePage(mikes);
        notePageManager.createNotePage(suzans);

        assertThat(suzans.getId()).isNotEqualTo(mikes.getId());
        assertThat(suzans.getNotes()).isNotNull();
        assertThat(mikes.getId()).isNotNull();

        assertThat(mikes).isEqualToComparingFieldByField(notePageManager.findNotePageById(mikes.getId()));
        assertThat(suzans).isEqualToComparingFieldByField(notePageManager.findNotePageById(suzans.getId()));
    }

    @Test
    public void createNotePageNullArgumentTest() {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> notePageManager.createNotePage(null));
    }

    @Test
    public void createNotePageInvalidAttributesTest() throws Exception {
        NotePage valid = mikeNotePage();
        NotePage invalidWidth = mikeNotePage();
        NotePage invalidHeight = mikeNotePage();

        invalidWidth.setWidth(0);
        invalidHeight.setHeight(0);

        notePageManager.createNotePage(valid);

        assertThatExceptionOfType(IllegalEntityException.class)
                .isThrownBy(() -> notePageManager.createNotePage(invalidWidth));
        assertThatExceptionOfType(IllegalEntityException.class)
                .isThrownBy(() -> notePageManager.createNotePage(invalidHeight));
    }

    @Test
    public void createNotePageValidationTest() throws Exception {
        NotePage signedId = mikeNotePage();
        signedId.setId(1L);

        assertThatExceptionOfType(ValidationException.class)
                .isThrownBy(() -> notePageManager.createNotePage(signedId));
    }

    @FunctionalInterface
    private interface Operation<T> {
        void callOn(T subjectOfOperation) throws Exception;
    }

    private void testEditNotePage(Operation<NotePage> updateOperation) throws Exception {
        NotePage notePageForEdit = mikeNotePage();
        NotePage anotherNotePage = suzanNotePage();

        notePageManager.createNotePage(notePageForEdit);
        notePageManager.createNotePage(anotherNotePage);

        updateOperation.callOn(notePageForEdit);

        notePageManager.updateNotePage(notePageForEdit);

        assertThat(notePageManager.findNotePageById(notePageForEdit.getId()))
                .isEqualToComparingFieldByField(notePageForEdit);
        assertThat(notePageManager.findNotePageById(anotherNotePage.getId()))
                .isEqualToComparingFieldByField(anotherNotePage);
    }

    @Test
    public void editUserTest() throws Exception {
        NoteUser user = new NoteUser.Builder()
                .userName("mark")
                .emailAddress("new@mail.com")
                .passwordHash("dfdfdf")
                .build();
        userManager.createUser(user);
        testEditNotePage((notePage) -> notePage.setNoteUser(user));
    }

    @Test
    public void editColorTest() throws Exception {
        testEditNotePage((notePage) -> notePage.setColor("#518835"));
    }

    @Test
    public void editNotesTest() throws Exception {
        testEditNotePage((notePage) ->
                notePage.setNotes(Arrays.asList(
                        new Note(null, "aaaaafdr"),
                        new Note(null, "bbbbbbf"))));
    }

    @Test
    public void editWidth() throws Exception {
        testEditNotePage((notePage) -> notePage.setWidth(555));
    }

    @Test
    public void editHeight() throws Exception {
        testEditNotePage((notePage) -> notePage.setHeight(555));
    }

    @Test
    public void editUserIllegalEntityTest() throws Exception {
        NotePage invalidWidth = mikeNotePage();
        NotePage invalidHeight = mikeNotePage();
        NotePage invalidPerHorizontal = mikeNotePage();
        NotePage invalidPerVertical = mikeNotePage();

        notePageManager.createNotePage(invalidWidth);
        notePageManager.createNotePage(invalidHeight);
        notePageManager.createNotePage(invalidPerHorizontal);
        notePageManager.createNotePage(invalidPerVertical);

        invalidWidth.setWidth(0);
        invalidHeight.setHeight(0);

        assertThatExceptionOfType(IllegalEntityException.class)
                .isThrownBy(() -> notePageManager.updateNotePage(invalidWidth));
        assertThatExceptionOfType(IllegalEntityException.class)
                .isThrownBy(() -> notePageManager.updateNotePage(invalidHeight));
    }

    @Test
    public void editNotePageValidationTest() throws Exception {
        NotePage mikes = mikeNotePage();
        NotePage suzans = suzanNotePage();

        assertThatExceptionOfType(ValidationException.class)
                .isThrownBy(() -> notePageManager.updateNotePage(mikes));

        notePageManager.createNotePage(suzans);
        suzans.setId(5L);

        assertThatExceptionOfType(ValidationException.class)
                .isThrownBy(() -> notePageManager.createNotePage(suzans));
    }

    @Test
    public void deleteNotePageTest() throws Exception {
        NotePage mikes = mikeNotePage();
        NotePage suzans = suzanNotePage();
        NotePage petes = peteNotePage();

        assertThat(notePageManager.getAllNotePages()).isEmpty();

        notePageManager.createNotePage(mikes);
        notePageManager.createNotePage(suzans);
        notePageManager.createNotePage(petes);

        assertThat(notePageManager.getAllNotePages()).containsExactly(mikes, suzans, petes);

        notePageManager.deleteNotePage(petes);

        assertThat(notePageManager.getAllNotePages()).containsExactly(mikes, suzans);

        notePageManager.deleteNotePage(mikes);
        notePageManager.deleteNotePage(suzans);

        assertThat(notePageManager.getAllNotePages()).isEmpty();
    }

    @Test
    public void deleteNotePageNullArgumentTest() throws Exception {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> notePageManager.deleteNotePage(null));
    }

    @Test
    public void deleteNotePageNullIdTest() throws Exception {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> notePageManager.deleteNotePage(null));
    }

    @Test
    public void deleteNotePageNonExistingNotePageTest() throws Exception {
        NotePage page = mikeNotePage();
        page.setId(1L);

        assertThatExceptionOfType(ValidationException.class)
                .isThrownBy(() -> notePageManager.deleteNotePage(page));
    }

    @Test
    public void findNotePageByIdTest() throws Exception {
        NotePage mikes = mikeNotePage();
        NotePage suzans = suzanNotePage();
        NotePage petes = peteNotePage();

        notePageManager.createNotePage(mikes);
        notePageManager.createNotePage(suzans);
        notePageManager.createNotePage(petes);

        Long id = suzans.getId();

        assertThat(notePageManager.findNotePageById(id)).isEqualToComparingFieldByField(suzans);
    }

    @Test
    public void findNotePageByIdNullArgumentTest() throws Exception {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> notePageManager.findNotePageById(null));
    }

    @Test
    public void findNotePageById() throws Exception {
        assertThat(notePageManager.findNotePageById(1L)).isNull();
    }

    @Test
    public void getNotePagesForUserTest() throws Exception {
        NotePage mikes1 = mikeNotePage();
        NotePage mikes2 = mikeNotePage();
        NotePage suzans = suzanNotePage();
        mikes2.setColor("#512785");

        notePageManager.createNotePage(mikes1);
        notePageManager.createNotePage(mikes2);
        notePageManager.createNotePage(suzans);

        assertThat(notePageManager.getNotePagesForUser(mike)).containsExactly(mikes1, mikes2);
    }

    @Test
    public void getNotePagesForUserNullArgumentTest() throws Exception {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> notePageManager.getNotePagesForUser(null));
    }

    @Test
    public void getNotePagesForUserNonExistingUser() throws Exception {
        NoteUser nonExistingUser = new NoteUser.Builder()
                .id(55L)
                .userName("noteUser")
                .emailAddress("noteUser@gmail.com")
                .passwordHash("dfdfdfdf")
                .build();
        assertThatExceptionOfType(ValidationException.class)
                .isThrownBy(() -> notePageManager.getNotePagesForUser(nonExistingUser));
    }
}
