import cz.tyckouni.development.notter.network.Entities.NoteUser;
import cz.tyckouni.development.notter.network.common.DuplicateEmailException;
import cz.tyckouni.development.notter.network.common.DuplicateUserNameEception;
import cz.tyckouni.development.notter.network.common.IllegalEntityException;
import cz.tyckouni.development.notter.network.common.ValidationException;
import cz.tyckouni.development.server.Managers.UserManagerImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;


import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

/**
 * Test class for {@link NoteUserManagerImplTest}
 */
public class NoteUserManagerImplTest {
    private EmbeddedDatabase db;
    private UserManagerImpl userManager;

    private NoteUser mikeUser() {
        return new NoteUser.Builder()
                .userName("Mike")
                .emailAddress("test@seznam.cz")
                .passwordHash("54a12e3c5d349e5d3c8b4b")
                .build();
    }

    private NoteUser peteUser() {
        return new NoteUser.Builder()
                .userName("Pete")
                .emailAddress("pete@tyckouni.cz")
                .passwordHash("5564b68632519d+e51321a")
                .build();
    }

    private NoteUser steveUser() {
        return new NoteUser.Builder()
                .userName("Steve")
                .emailAddress("steve@tyckouni.cz")
                .passwordHash("5564bd8632519d+e51321a")
                .build();
    }

    @Before
    public void setUp() throws Exception {
        db = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.DERBY)
                .addScript("test-database.sql")
                .build();

        userManager = new UserManagerImpl(db);
    }

    @After
    public void tearDown() throws Exception {
        db.shutdown();
    }

    @Test
    public void createUserTest() throws Exception {
        NoteUser mike = mikeUser();
        NoteUser pete = peteUser();

        userManager.createUser(mike);
        userManager.createUser(pete);

        assertThat(mike.getId()).isNotEqualTo(pete.getId());
        assertThat(mike.getId()).isNotNull();
        assertThat(pete.getId()).isNotNull();

        assertThat(mike).isEqualToComparingFieldByField(userManager.findUserById(mike.getId()));
        assertThat(pete).isEqualToComparingFieldByField(userManager.findUserById(pete.getId()));
    }

    @Test
    public void createUserNullArgumentTest() {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> userManager.createUser(null));
    }

    @Test
    public void createUserInvalidAttributesTest() throws Exception {
        NoteUser validUser = peteUser();
        NoteUser invalidEmailUser = mikeUser();
        NoteUser invalidUsernameUser = mikeUser();
        NoteUser invalidPasswordHashUser = peteUser();

        invalidEmailUser.setEmailAddress(null);
        validUser.setEmailAddress("valid@valid.cz");
        invalidUsernameUser.setUserName(null);
        invalidPasswordHashUser.setPasswordHash(null);

        userManager.createUser(validUser);

        assertThatExceptionOfType(IllegalEntityException.class)
                .isThrownBy(() -> userManager.createUser(invalidEmailUser));
        assertThatExceptionOfType(IllegalEntityException.class)
                .isThrownBy(() -> userManager.createUser(invalidUsernameUser));
        assertThatExceptionOfType(IllegalEntityException.class)
                .isThrownBy(() -> userManager.createUser(invalidPasswordHashUser));
    }

    @Test
    public void createUserTestDuplicateEmailTest() throws Exception {
        NoteUser mike = mikeUser();
        NoteUser pete = peteUser();

        pete.setEmailAddress(mike.getEmailAddress());

        userManager.createUser(mike);

        assertThatExceptionOfType(DuplicateEmailException.class)
                .isThrownBy(() -> userManager.createUser(pete));
    }

    @Test
    public void createUserTestDuplicateUserNameTest() throws Exception {
        NoteUser mike = mikeUser();
        NoteUser pete = peteUser();

        pete.setUserName(mike.getUserName());

        userManager.createUser(mike);

        assertThatExceptionOfType(DuplicateUserNameEception.class)
                .isThrownBy(() -> userManager.createUser(pete));
    }

    @Test
    public void createUserTestSignedIdTest() throws Exception {
        NoteUser mike = mikeUser();
        mike.setId(1L);

        assertThatExceptionOfType(ValidationException.class)
                .isThrownBy(() -> userManager.createUser(mike));
    }

    @FunctionalInterface
    private interface Operation<T> {
        void callOn(T subjectOfOperation) throws Exception;
    }

    private void testEditUser(Operation<NoteUser> updateOperation) throws Exception {
        NoteUser userForEdit = mikeUser();
        NoteUser anotherUser = peteUser();

        userManager.createUser(userForEdit);
        userManager.createUser(anotherUser);

        updateOperation.callOn(userForEdit);

        userManager.updateUser(userForEdit);

        assertThat(userManager.findUserById(userForEdit.getId()))
                .isEqualToComparingFieldByField(userForEdit);
        assertThat(userManager.findUserById(anotherUser.getId()))
                .isEqualToComparingFieldByField(anotherUser);
    }

    @Test
    public void editUserNameTest() throws Exception {
        testEditUser((user) -> user.setUserName("New Name"));
    }

    @Test
    public void editUserEmailTest() throws Exception {
        testEditUser((user) -> user.setEmailAddress("new@mail.com"));
    }

    @Test
    public void editUserPasswordHashTest() throws Exception {
        testEditUser((user) -> user.setPasswordHash("5f86f8786e54f865c8dc52a"));
    }

    @Test
    public void editUserNullArgumentTest() throws Exception {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> userManager.updateUser(null));
    }

    @Test
    public void editUserIllegalEntityTest() throws Exception {
        NoteUser pete = peteUser();
        NoteUser mike = mikeUser();
        NoteUser steve = steveUser();

        userManager.createUser(pete);
        userManager.createUser(mike);
        userManager.createUser(steve);

        mike.setUserName(null);
        assertThatExceptionOfType(IllegalEntityException.class)
                .isThrownBy(() -> userManager.updateUser(mike));

        steve.setPasswordHash(null);
        assertThatExceptionOfType(IllegalEntityException.class)
                .isThrownBy(() -> userManager.updateUser(steve));
    }

    @Test
    public void editUserValidationTest() throws Exception {
        NoteUser steve = steveUser();
        NoteUser mike = mikeUser();

        assertThatExceptionOfType(ValidationException.class)
                .isThrownBy(() -> userManager.updateUser(mike));

        userManager.createUser(steve);
        steve.setId(599L);

        assertThatExceptionOfType(ValidationException.class)
                .isThrownBy(() -> userManager.updateUser(steve));
    }

    @Test
    public void editDuplicateUserNameTest() throws Exception {
        NoteUser pete = peteUser();
        NoteUser steve = steveUser();

        userManager.createUser(pete);
        userManager.createUser(steve);

        steve.setUserName(pete.getUserName());
        assertThatExceptionOfType(DuplicateUserNameEception.class)
                .isThrownBy(() -> userManager.updateUser(steve));
    }

    @Test
    public void editDuplicateEmailTest() throws Exception {
        NoteUser pete = peteUser();
        NoteUser steve = steveUser();

        userManager.createUser(pete);
        userManager.createUser(steve);

        steve.setEmailAddress(pete.getEmailAddress());
        assertThatExceptionOfType(DuplicateEmailException.class)
                .isThrownBy(() -> userManager.updateUser(steve));
    }

    @Test
    public void deleteUserTest() throws Exception {
        NoteUser mike = mikeUser();
        NoteUser pete = peteUser();
        NoteUser steve = steveUser();

        assertThat(userManager.getAllUsers()).isEmpty();

        userManager.createUser(mike);
        userManager.createUser(pete);
        userManager.createUser(steve);

        assertThat(userManager.getAllUsers()).containsOnly(mike, pete, steve);

        userManager.deleteUser(steve);

        assertThat(userManager.getAllUsers()).containsOnly(mike, pete);
    }

    @Test
    public void deleteUserNullArgumentTest() throws Exception {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> userManager.updateUser(null));
    }

    @Test
    public void deleteUserNonExistingUserTest() throws Exception {
        NoteUser mike = mikeUser();
        mike.setId(1L);

        assertThatExceptionOfType(ValidationException.class)
                .isThrownBy(() -> userManager.deleteUser(mike));
    }

    @Test
    public void deleteUserNoIdTest() throws Exception {
        NoteUser mike = mikeUser();

        assertThatExceptionOfType(ValidationException.class)
                .isThrownBy(() -> userManager.deleteUser(mike));
    }

    @Test
    public void findUserByIdTest() throws Exception {
        NoteUser mike = mikeUser();
        NoteUser pete = peteUser();

        userManager.createUser(mike);
        userManager.createUser(pete);

        Long mikeId = mike.getId();
        Long peteId = pete.getId();
        assertThat(mikeId).isNotNull();
        assertThat(peteId).isNotNull();

        assertThat(mike).isNotSameAs(userManager.findUserById(mikeId));
        assertThat(mike).isEqualToComparingFieldByField(userManager.findUserById(mikeId));
        assertThat(pete).isNotSameAs(userManager.findUserById(peteId));
        assertThat(pete).isEqualToComparingFieldByField(userManager.findUserById(peteId));
    }

    @Test
    public void getAllUsersTest() throws Exception {
        NoteUser mike = mikeUser();
        NoteUser pete = peteUser();
        NoteUser steve = steveUser();

        assertThat(userManager.getAllUsers()).isEmpty();

        userManager.createUser(steve);

        assertThat(userManager.getAllUsers()).containsOnly(steve);

        userManager.createUser(pete);
        userManager.createUser(mike);

        assertThat(userManager.getAllUsers()).containsOnly(steve, pete, mike);

        userManager.deleteUser(steve);

        assertThat(userManager.getAllUsers()).containsOnly(pete, mike);
    }
}
