import cz.tyckouni.development.notter.network.Entities.NoteUser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;


/**
 * Test class for testing {@link cz.tyckouni.development.server.ConnectionHandler}
 */
public class ConnectionHandlerTest {

    private EmbeddedDatabase db;

    private NoteUser mikeUser() {
        return new NoteUser.Builder()
                .userName("Mike")
                .emailAddress("test@seznam.cz")
                .passwordHash("54a12e3c5d349e5d3c8b4b")
                .build();
    }

    @Before
    public void setUp() throws Exception {
        db = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.DERBY)
                .addScript("test-database.sql")
                .build();
    }

    @After
    public void tearDown() throws Exception {
        db.shutdown();
    }
}
