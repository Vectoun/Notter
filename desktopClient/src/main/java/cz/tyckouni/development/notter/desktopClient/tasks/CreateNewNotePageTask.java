package cz.tyckouni.development.notter.desktopClient.tasks;

import cz.tyckouni.development.notter.desktopClient.NotterClient;
import cz.tyckouni.development.notter.desktopClient.Controllers.NotePageController;
import cz.tyckouni.development.notter.network.Entities.NotePage;
import cz.tyckouni.development.notter.network.ServerAction;
import cz.tyckouni.development.notter.network.ServerResponse;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

/**
 * Task class for creating new NotePage in DB
 */
public class CreateNewNotePageTask extends Task<NotePage> {
    private final static Logger log = LoggerFactory.getLogger(UploadNotePageTask.class);


    private NotePageController notePageController;
    private NotePage notePage;

    public CreateNewNotePageTask(NotePageController notePageController, NotePage notePage) {
        this.notePageController = notePageController;
        this.notePage = notePage;
    }

    @Override
    protected NotePage call() throws Exception {
        Properties serverProperties = ConnectionUtilities.getServerProperties();

        try (Socket socket = new Socket()) {
            NotePage result;
            socket.connect(new InetSocketAddress(serverProperties.getProperty("SERVER_NAME"),
                            Integer.parseInt(serverProperties.getProperty("APPLICATION_PORT"))),
                    Integer.parseInt(serverProperties.getProperty("CONNECTION_TIMEOUT")));

            ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream input = new ObjectInputStream(socket.getInputStream());

            if (NotterClient.getNoteUser() == null) {
                socket.close();
                log.error("NoteUser is null when trying to upload NotePage data");
                throw new IllegalStateException("NoteUser is null when trying to upload NotePage data");
            }
            output.writeObject(ServerAction.NEW_NOTE_PAGE);
            output.writeObject(NotterClient.getNoteUser());
            output.writeObject(new NotePage.Builder()
                    .color("#11b0a5")
                    .height(200)
                    .width(200)
                    .notes(new ArrayList<>())
                    .noteUser(NotterClient.getNoteUser())
                    .build());
            output.flush();

            if (input.readObject() == ServerResponse.SUCCESS) {
                result = (NotePage)input.readObject();
            } else {
                result = null;
            }

            output.writeObject(ServerAction.CLOSE);
            output.flush();

            return result;
        }
    }

    @Override
    protected void succeeded() {
        try {
            NotePage notePage = get();
            if(notePage != null) {
                NotePageController.addNotePage(notePage);
            } else {
                showAlert();
            }
        } catch (InterruptedException e) {
            throw new AssertionError(e);
        } catch (ExecutionException e) {
            log.error("Failed to download NotePage:" + notePage, e);
            showAlert();
        } finally {
            notePageController.setProcessingState(false);
        }
    }

    @Override
    protected void failed() {
        log.error("Failed to download NotePage: " + notePage);
        showAlert();
        notePageController.setProcessingState(false);
    }

    private void showAlert() {
        Alert alert = new Alert(Alert.AlertType.ERROR, "Failed to download data. Please try it again later.");
        alert.showAndWait()
                .filter(response -> response == ButtonType.OK);
    }
}
