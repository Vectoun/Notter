package cz.tyckouni.development.notter.desktopClient.tasks;

import cz.tyckouni.development.notter.desktopClient.NotterClient;
import cz.tyckouni.development.notter.desktopClient.Controllers.NotePageController;
import cz.tyckouni.development.notter.network.Entities.NotePage;
import cz.tyckouni.development.notter.network.ServerAction;
import cz.tyckouni.development.notter.network.ServerResponse;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

/**
 * Task class for uploading {@link NotePage} data
 */
public class UploadNotePageTask extends Task<Boolean> {

    private final static Logger log = LoggerFactory.getLogger(UploadNotePageTask.class);

    private NotePageController notePageController;
    private NotePage newNotePage;

    public UploadNotePageTask(NotePageController notePageController, NotePage newNotePage) {
        assert notePageController != null;
        assert newNotePage        != null;

        this.notePageController = notePageController;
        this.newNotePage = newNotePage;
    }

    @Override
    protected Boolean call() throws Exception {
        Properties serverProperties = ConnectionUtilities.getServerProperties();

        try (Socket socket = new Socket()) {
            Boolean result;

            socket.connect(new InetSocketAddress(serverProperties.getProperty("SERVER_NAME"),
                            Integer.parseInt(serverProperties.getProperty("APPLICATION_PORT"))),
                    Integer.parseInt(serverProperties.getProperty("CONNECTION_TIMEOUT")));

            ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream input = new ObjectInputStream(socket.getInputStream());

            output.writeObject(ServerAction.UPDATE_NOTE_PAGE);
            output.writeObject(NotterClient.getNoteUser());
            output.writeObject(newNotePage);
            output.flush();

            if (input.readObject() == ServerResponse.SUCCESS) {
                notePageController.setCurrentNotePage((NotePage) input.readObject());

                output.writeObject(ServerAction.CLOSE);
                output.flush();

                result = true;
            } else {
                output.writeObject(ServerAction.CLOSE);
                output.flush();

                result = false;
            }

            output.writeObject(ServerAction.CLOSE);
            output.flush();

            return result;
        }
    }

    @Override
    protected void succeeded() {
        try {
            if(!get()) {
                showAlert();
            }
        } catch (InterruptedException e) {
            throw new AssertionError();
        } catch (ExecutionException e) {
            log.error("Failed to upload NotePage: " + newNotePage, e.getCause());
            System.err.println(e.getCause().toString());
            showAlert();
        } finally {
            notePageController.setProcessingState(false);
        }
    }

    @Override
    protected void failed() {
        log.error("Failed to upload NotePage: " + newNotePage);
        showAlert();
        notePageController.setProcessingState(false);
    }

    private void showAlert() {
        Alert alert = new Alert(Alert.AlertType.ERROR, "Failed to upload data. Please try it again later.");
        alert.showAndWait()
                .filter(response -> response == ButtonType.OK);
    }
}
