package cz.tyckouni.development.notter.desktopClient;

/**
 * Created by Vectoun on 4. 6. 2017.
 */
public enum ButtonColor {
    DARK_BLUE("#1f3a82", "icons/color-button-dark-blue.png"),
    BROWN("#7e5743", "icons/color-button-brown.png"),
    PINK("#cc156f", "icons/color-button-pink.png"),
    PURPLE("#742889", "icons/color-button-purple.png"),
    LIGHT_BLUE("#00a6e4", "icons/color-button-light-blue.png"),
    GREEN("#3da646", "icons/color-button-green.png"),
    YELLOW("#f3e700", "icons/color-button-yellow.png"),
    RED("#c82128", "icons/color-button-red.png"),
    THEME("#3ca695", "icons/color-button-theme.png");

    private final String color;
    private final String iconPath;

    ButtonColor(String color, String iconPath) {
        this.color = color;
        this.iconPath = iconPath;
    }
    @Override
    public String toString() {
        return color;
    }

    public String getIconPath() {
        return iconPath;
    }
}
