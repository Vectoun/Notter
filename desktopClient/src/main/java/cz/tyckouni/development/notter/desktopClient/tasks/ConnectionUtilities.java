package cz.tyckouni.development.notter.desktopClient.tasks;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Properties;

/**
 * Class for handling connection to server
 */
public class ConnectionUtilities {

    private static final Properties serverProperties;
    private static final org.slf4j.Logger log;

    static {
        log = org.slf4j.LoggerFactory.getLogger(ConnectionUtilities.class);

        serverProperties = new Properties();
        try {
            serverProperties.load(ConnectionUtilities.class.getClassLoader()
                    .getResourceAsStream("server-config.properties"));
        } catch (IOException e) {
            throw new RuntimeException("Failed to open 'server-config.properties file", e);
        }
    }

    public static Properties getServerProperties() {
        return serverProperties;
    }
}
