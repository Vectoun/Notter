package cz.tyckouni.development.notter.desktopClient.tasks;

import cz.tyckouni.development.notter.desktopClient.NotterClient;
import cz.tyckouni.development.notter.desktopClient.Controllers.NotePageController;
import cz.tyckouni.development.notter.network.Entities.NotePage;
import cz.tyckouni.development.notter.network.ServerAction;
import cz.tyckouni.development.notter.network.ServerResponse;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

/**
 * Task class for deleting {@link NotePage} data from DB
 */
public class DeleteNotePageTask extends Task<Boolean> {

    private final static Logger log = LoggerFactory.getLogger(DownloadNotePageTask.class);

    private NotePageController notePageController;
    private NotePage notePage;

    public DeleteNotePageTask(NotePageController notePageController, NotePage notePage) {
        this.notePageController = notePageController;
        this.notePage = notePage;
    }

    @Override
    protected Boolean call() throws Exception {

        Properties serverProperties = ConnectionUtilities.getServerProperties();

        try (Socket socket = new Socket()) {
            if (NotterClient.getNoteUser() == null) {
                log.error("NoteUser is null when trying to upload NotePage data");
                throw new IllegalStateException("NoteUser is null when trying to upload NotePage data");
            }

            socket.connect(new InetSocketAddress(serverProperties.getProperty("SERVER_NAME"),
                            Integer.parseInt(serverProperties.getProperty("APPLICATION_PORT"))),
                    Integer.parseInt(serverProperties.getProperty("CONNECTION_TIMEOUT")));

            ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream input = new ObjectInputStream(socket.getInputStream());

            output.writeObject(ServerAction.DELETE_PAGE);
            output.writeObject(NotterClient.getNoteUser());
            output.writeObject(notePage);
            output.flush();
            Boolean result = input.readObject() == ServerResponse.SUCCESS;

            output.writeObject(ServerAction.CLOSE);
            output.flush();

            return result;
        }
    }

    @Override
    protected void succeeded() {
        try {
            if(get()) {
                notePageController.onCloseAction();
            } else {
                showAlert();
            }
        } catch (InterruptedException e) {
            throw new AssertionError();
        } catch (ExecutionException e) {
            log.error("Failed to to download NotePage data", e);
            notePageController.setProcessingState(false);
            showAlert();
        } finally {
            notePageController.setProcessingState(false);
        }
    }

    @Override
    protected void failed() {
        showAlert();
        notePageController.setProcessingState(false);
    }

    private void showAlert() {
        Alert alert = new Alert(Alert.AlertType.ERROR, "Failed to remove page. Please try it again later.");
        alert.showAndWait()
                .filter(response -> response == ButtonType.OK);
    }
}
