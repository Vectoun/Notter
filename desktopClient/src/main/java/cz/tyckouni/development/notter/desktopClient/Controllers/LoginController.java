package cz.tyckouni.development.notter.desktopClient.Controllers;

import cz.tyckouni.development.notter.desktopClient.NotterClient;
import cz.tyckouni.development.notter.desktopClient.tasks.AuthenticationTask;
import cz.tyckouni.development.notter.network.Entities.NoteUser;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;


/**
 * Controller class for login screen
 */
public class LoginController {

    private final static Logger log = LoggerFactory.getLogger(LoginController.class);

    @FXML private Label statusLabel;
    @FXML private TextField userNameTextField;
    @FXML private PasswordField passwordField;
    @FXML private Button loginButton;
    @FXML private Button registerButton;
    @FXML private AnchorPane mainAnchorPane;

    private String hash(String password) {
        return password; //TODO:
    }

    private NoteUser getUserFromForm() {
        return new NoteUser.Builder()
                .userName(userNameTextField.getText())
                .passwordHash(hash(passwordField.getText()))
                .build();
    }

    @FXML
    protected void onLoginButtonAction() {
        AuthenticationTask authenticationTask = new AuthenticationTask(getUserFromForm(),
                (Stage) mainAnchorPane.getScene().getWindow());
        authenticationTask.setOnFailedAction(() -> Platform.runLater(() -> this.setProcessingState(false)));

        setProcessingState(true);
        NotterClient.execute(authenticationTask);
    }

    @FXML
    protected void onRegisterButtonAction() {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/layouts/RegisterLayout.fxml"));

        Stage stage = new Stage(StageStyle.DECORATED);

        try {
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
        } catch (IOException e) {
            log.error("Failed to load RegisterLayout.fxml");
            throw new AssertionError("Failed to load RegisterLayout.fxml", e);
        }

        RegisterController controller = loader.getController();



    }

    private void setProcessingState(boolean value) {
        if (value) {
            statusLabel.setText("Processing...");
        } else {
            statusLabel.setText("");
        }
        userNameTextField.setDisable(value);
        passwordField.setDisable(value);
        loginButton.setDisable(value);
        registerButton.setDisable(value);
        mainAnchorPane.setDisable(value);
    }
}
