package cz.tyckouni.development.notter.desktopClient.Controllers;

import cz.tyckouni.development.notter.desktopClient.ButtonColor;
import cz.tyckouni.development.notter.desktopClient.NotterClient;
import cz.tyckouni.development.notter.desktopClient.tasks.CreateNewNotePageTask;
import cz.tyckouni.development.notter.desktopClient.tasks.DeleteNotePageTask;
import cz.tyckouni.development.notter.desktopClient.tasks.DownloadNotePageTask;
import cz.tyckouni.development.notter.desktopClient.tasks.UploadNotePageTask;
import cz.tyckouni.development.notter.network.Entities.Note;
import cz.tyckouni.development.notter.network.Entities.NotePage;
import cz.tyckouni.development.notter.network.Entities.NoteUser;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * @author Vojtech Sassmann
 */
public class NotePageController {

    private final static Logger log = LoggerFactory.getLogger(NotePageController.class);

    private static int windowCounter = 0;

    private NotePage notePage;
    private Stage stage;
    private NoteUser noteUser;

    private double xOffset = 0;
    private double yOffset = 0;


    @FXML private TextArea noteTextArea;
    @FXML private AnchorPane processingAnchorPane;
    @FXML private Button uploadButton;
    @FXML private Button downloadButton;
    @FXML private Button removePageButton;
    @FXML private Button addPageButton;

    @FXML private AnchorPane themeAnchorPane1;
    @FXML private AnchorPane themeAnchorPane2;
    @FXML private Polygon themePolygon;


    private static void createNotePageStage(NotePage notePage) {
        NoteUser noteUser = notePage.getNoteUser();
        FXMLLoader loader = new FXMLLoader(NotterClient.class.getResource("/layouts/NotePageLayout.fxml"));

        Stage stage = new Stage(StageStyle.TRANSPARENT);

        try {
            Scene scene = new Scene(loader.load());
            scene.setFill(Color.TRANSPARENT);
            stage.setScene(scene);
        } catch (IOException e) {
            log.error("Failed to load NotePageLayout", e);
            throw new AssertionError("Failed to load notePageLayout", e);
        }

        NotePageController controller = loader.getController();
        controller.init(notePage, noteUser, stage);

        windowCounter++;

        stage.show();
    }

    public static void addNotePage(NotePage notePage) {
        createNotePageStage(notePage);
    }

    private void init(NotePage notePage, NoteUser noteUser, Stage stage) {
        this.notePage = notePage;
        this.noteUser = noteUser;

        noteTextArea.setText(unParseNotes(notePage));
        setStyleColor(notePage.getColor());

        /* TOBE implemented in future
        mainAnchorPane.setPrefWidth(notePage.getWidth());
        mainAnchorPane.setPrefHeight(notePage.getHeight());
         */

        this.stage = stage;
    }

    private void setStyleColor(String value) {
        String oldColor = themeAnchorPane1.getStyle()
                .replaceAll(".*-fx-background-color: ", "")
                .replaceAll("^#[a-f0-9]^6", "");
        System.out.println(oldColor);

        themeAnchorPane1.setStyle("-fx-background-color: " + value);
        System.out.println("style" + themeAnchorPane1.getStyle());
        themeAnchorPane2.setStyle("-fx-background-color: " + value);
        themePolygon.setStyle("-fx-fill: " + value);
    }

    @FXML
    protected void onMousePressed(MouseEvent mouseEvent) {
        xOffset = mouseEvent.getSceneX();
        yOffset = mouseEvent.getSceneY();
    }

    @FXML
    protected void onMouseDragged(MouseEvent mouseEvent) {
        stage.setX(mouseEvent.getScreenX() - xOffset);
        stage.setY(mouseEvent.getScreenY() - yOffset);
    }

    @FXML
    protected void onDownloadButtonAction() {
        setProcessingState(true);

        Thread thread = new Thread(new DownloadNotePageTask(this, notePage, noteUser));
        thread.setDaemon(true);
        NotterClient.execute(thread);
    }

    @FXML
    protected void onUploadButtonAction() {
        setProcessingState(true);

        Thread thread = new Thread(new UploadNotePageTask(this, getPageFromUI()));
        thread.setDaemon(true);
        NotterClient.execute(thread);
    }

    @FXML
    public void onCloseAction() {
        stage.close();
        if(--windowCounter == 0) {
            System.exit(0);
        }
    }

    @FXML
    protected void onNewNoteAction() {
        setProcessingState(true);

        Thread thread = new Thread(new CreateNewNotePageTask(this, notePage));
        thread.setDaemon(true);
        NotterClient.execute(thread);
    }

    //TODO
    /*
    @FXML
    protected void onExitAction() {
        System.exit(0);
    }
*/
    @FXML
    protected void onRemovePageAction() {
        ButtonType yes = new ButtonType("Yes", ButtonBar.ButtonData.YES);
        ButtonType no = new ButtonType("No", ButtonBar.ButtonData.NO);
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.getDialogPane().getButtonTypes().add(yes);
        dialog.getDialogPane().getButtonTypes().add(no);
        dialog.setContentText("Are you sure that you want to remove this page? All data will be lost.");
        Optional<ButtonType> result = dialog.showAndWait();
        if(result.isPresent()) {
            if (result.get() == yes) {
                Thread thread = new Thread(new DeleteNotePageTask(this, notePage));
                thread.setDaemon(true);
                NotterClient.execute(thread);
            }
        }
    }

    @FXML
    protected void onChangeColorAction(ActionEvent event) {
        Button buttonPressed = (Button)event.getSource();
        String buttonColorString = (buttonPressed.styleProperty().toString()
                .replaceAll(".*button ", "")
                .replaceAll("].*", ""));
        ButtonColor buttonColor = ButtonColor.valueOf(buttonColorString.toUpperCase().replaceAll("-", "_"));
        setStyleColor(buttonColor.toString());
    }

    private String unParseNotes(NotePage notePage) {
        List<Note> notes = notePage.getNotes();
        StringBuilder result = new StringBuilder();
        for(Note note : notes) {
            result.append(note.getData()).append(System.lineSeparator());
        }
        return result.toString();
    }

    private NotePage getPageFromUI() {
        return new NotePage.Builder()
                .height((int)stage.getHeight())
                .width((int)stage.getWidth())
                .color(getActualColor())
                .noteUser(noteUser)
                .notes(parseNotes())
                .id(this.notePage.getId())
                .build();
    }

    private String getActualColor() {
        return themeAnchorPane1.getStyle()
                .replace("-fx-background-color: ", "")
                .substring(0, 7);
    }

    private List<Note> parseNotes() {
        List<Note> notes = new ArrayList<>();
        String[] lines = noteTextArea.getText().split(System.lineSeparator());
        for(String s : lines) {
            if(s.length() > 0) {
                notes.add(new Note(null, s));
            }
        }
        return notes;
    }

    public void setProcessingState(boolean value) {
        processingAnchorPane.setVisible(value);
        noteTextArea.setEditable(!value);
        uploadButton.setDisable(value);
        downloadButton.setDisable(value);
        addPageButton.setDisable(value);
        removePageButton.setDisable(value);
    }

    public void setCurrentNotePage(NotePage notePage) {
        init(notePage, noteUser, stage);
    }

}
