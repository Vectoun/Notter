package cz.tyckouni.development.notter.desktopClient.tasks;

import cz.tyckouni.development.notter.desktopClient.NotterClient;
import cz.tyckouni.development.notter.network.Entities.NoteUser;
import cz.tyckouni.development.notter.network.ServerAction;
import cz.tyckouni.development.notter.network.ServerResponse;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

/**
 * Task class to perform server authentication
 */
public class AuthenticationTask extends Task<Boolean> {

    private final static Logger log = LoggerFactory.getLogger(AuthenticationTask.class);

    private Runnable onFailedAction;

    private boolean success = false;

    private NoteUser noteUser;
    private Stage mainStage;

    public AuthenticationTask(NoteUser noteUser, Stage mainStage) {
        assert noteUser != null;
        assert mainStage != null;

        this.noteUser = noteUser;
        this.mainStage = mainStage;
    }

    @Override
    protected Boolean call() throws Exception {
        Boolean result;

        Properties serverProperties = ConnectionUtilities.getServerProperties();
        Socket socket = new Socket();

        socket.connect(new InetSocketAddress(serverProperties.getProperty("SERVER_NAME"),
                        Integer.parseInt(serverProperties.getProperty("APPLICATION_PORT"))),
                Integer.parseInt(serverProperties.getProperty("CONNECTION_TIMEOUT")));

        ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
        ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());

        output.writeObject(ServerAction.VERIFY_NOTE_USER);
        output.writeObject(noteUser);
        output.flush();

        ServerResponse response = (ServerResponse) input.readObject();

        if (response == ServerResponse.SUCCESS) {
            noteUser = (NoteUser) input.readObject();
            NotterClient.setNoteUser(noteUser);
            result = Boolean.TRUE;
        } else if (response == ServerResponse.FAILURE) {
            log.info("Invalid user name or password.");
            showAlert(Alert.AlertType.ERROR, "Invalid user name or password.");
            result = false;
        } else {
            log.info("Failed to connect to server during authentication");
            showAlert(Alert.AlertType.ERROR, "Failed to connect to server, please check your internet" +
                    " connection and try it again later.");
            result = false;
        }

        output.writeObject(ServerAction.CLOSE);
        output.flush();

        return result;
    }

    @Override
    protected void done() {
        boolean success = false;
        try {
            success = get();
            if (success) {
                DownloadUserDataTask downloadUserDataTask
                        = new DownloadUserDataTask(noteUser, mainStage);
                NotterClient.execute(downloadUserDataTask);
            }
        } catch (InterruptedException e) {
            throw new AssertionError();
        } catch (ExecutionException e) {
            showAlert(Alert.AlertType.ERROR, "Failed to contact server. Please try it again later.");
        } finally {
            if (!success) {
                if (onFailedAction != null) {
                    onFailedAction.run();
                }
            }
        }
    }



    private void showAlert(Alert.AlertType alertType, String message) {
        Platform.runLater(() -> {
            Alert alert = new Alert(alertType, message);
            alert.showAndWait()
                    .filter(response -> response == ButtonType.OK);
        });
    }

    public void setOnFailedAction(Runnable onFailedAction) {
        this.onFailedAction = onFailedAction;
    }
}
