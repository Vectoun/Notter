package cz.tyckouni.development.notter.desktopClient.tasks;

import cz.tyckouni.development.notter.desktopClient.Controllers.NotePageController;
import cz.tyckouni.development.notter.desktopClient.NotterClient;
import cz.tyckouni.development.notter.network.Entities.Note;
import cz.tyckouni.development.notter.network.Entities.NotePage;
import cz.tyckouni.development.notter.network.Entities.NoteUser;
import cz.tyckouni.development.notter.network.ServerAction;
import cz.tyckouni.development.notter.network.ServerResponse;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

/**
 * Worker class for logging noteUser to server and obtain his data
 */
public class DownloadUserDataTask extends Task<List<NotePage>> {

    private final static Logger log = LoggerFactory.getLogger(DownloadUserDataTask.class);

    private ObjectInputStream input;
    private ObjectOutputStream output;

    private NoteUser noteUser;
    private Stage stage;

    private Runnable onFinishedAction;

    public DownloadUserDataTask(NoteUser noteUser, Stage stage) {
        assert noteUser != null;
        assert stage != null;

        this.noteUser = noteUser;
        this.stage = stage;
    }

    @Override
    protected List<NotePage> call() throws Exception {
        Properties serverProperties = ConnectionUtilities.getServerProperties();

        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress(serverProperties.getProperty("SERVER_NAME"),
                                Integer.parseInt(serverProperties.getProperty("APPLICATION_PORT"))),
                        Integer.parseInt(serverProperties.getProperty("CONNECTION_TIMEOUT")));

            output = new ObjectOutputStream(socket.getOutputStream());
            input = new ObjectInputStream(socket.getInputStream());

            List<NotePage> result = getNotePages();

            output.writeObject(ServerAction.CLOSE);
            output.flush();

            return result;
        }
    }

    @SuppressWarnings("unchecked")
    //this suppress is ok because we read in this moment only objects of List<NotePage>
    private List<NotePage> getNotePages() throws IOException {
        output.writeObject(ServerAction.SEND_DATA);
        output.writeObject(noteUser);
        output.flush();

        try {
            if(input.readObject() == ServerResponse.SUCCESS) {
                return (List)input.<List<NotePage>>readObject();
            }
        } catch (ClassNotFoundException e) {
            log.error("Communication error", e);
        }
        return null;
    }

    @Override
    protected void failed() {
        try {
            get();
        } catch (InterruptedException e) {
            throw new AssertionError(e);
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                log.error("IO operation failed during connecting server to download user data", e);
                showAlert(Alert.AlertType.ERROR,
                        "IO operation failed during connecting server to download user data");
            } else {
                log.error("An unknown exception occurred during connecting server to download user data", e);
                showAlert(Alert.AlertType.ERROR,
                        "An unknown exception during connecting server to download user data.");
            }
        }
    }

    @Override
    protected void done() {
        try {
            List<NotePage> pages = get();
            if(pages != null) {
                Platform.runLater(() -> stage.close());
                for(NotePage page : pages) {
                    page.setNoteUser(noteUser);
                }
                Platform.runLater(() -> pages.forEach(NotePageController::addNotePage));
            } else {
                log.error("Failed to download notes data, pages are null");
                showAlert(Alert.AlertType.ERROR,
                        "Connection error, please try it again later.");
            }
        } catch (InterruptedException e) {
            throw new AssertionError();
        } catch (ExecutionException e) {
            log.error("An unknown exception oduring connecting server to download user data", e);
            showAlert(Alert.AlertType.ERROR,
                    "Connection error, please try it again later.");
        } finally {
            if (onFinishedAction != null) {
                onFinishedAction.run();
            }
        }
    }

    private void showAlert(Alert.AlertType alertType, String message) {
        Platform.runLater(() -> {
            Alert alert = new Alert(alertType, message);
            alert.showAndWait()
                    .filter(response -> response == ButtonType.OK);
        });
    }

    public void setOnFinishedAction(Runnable onFinishedAction) {
        this.onFinishedAction = onFinishedAction;
    }
}
