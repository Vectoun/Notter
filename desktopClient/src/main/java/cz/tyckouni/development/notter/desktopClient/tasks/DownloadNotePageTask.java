package cz.tyckouni.development.notter.desktopClient.tasks;

import cz.tyckouni.development.notter.desktopClient.Controllers.NotePageController;
import cz.tyckouni.development.notter.network.Entities.NotePage;
import cz.tyckouni.development.notter.network.Entities.NoteUser;
import cz.tyckouni.development.notter.network.ServerAction;
import cz.tyckouni.development.notter.network.ServerResponse;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

/**
 * Task class for downloading {@link NotePage} data
 */
public class DownloadNotePageTask extends Task<NotePage> {

    private final static Logger log = LoggerFactory.getLogger(DownloadNotePageTask.class);

    private NotePageController notePageController;
    private NotePage notePage;
    private NoteUser noteUser;

    public DownloadNotePageTask(NotePageController notePageController, NotePage notePage, NoteUser noteUser) {
        assert notePageController != null;
        assert notePage           != null;
        assert noteUser           != null;

        this.notePageController = notePageController;
        this.notePage = notePage;
        this.noteUser = noteUser;
    }

    @Override
    protected NotePage call() throws Exception {

        Properties serverProperties = ConnectionUtilities.getServerProperties();

        try (Socket socket = new Socket()) {
            NotePage result;

            socket.connect(new InetSocketAddress(serverProperties.getProperty("SERVER_NAME"),
                            Integer.parseInt(serverProperties.getProperty("APPLICATION_PORT"))),
                    Integer.parseInt(serverProperties.getProperty("CONNECTION_TIMEOUT")));

            ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream input = new ObjectInputStream(socket.getInputStream());

            output.writeObject(ServerAction.DOWNLOAD_PAGE);
            output.writeObject(noteUser);
            output.writeObject(notePage);
            output.flush();
            if (input.readObject() == ServerResponse.SUCCESS) {
              result = (NotePage) input.readObject();
            } else {
                result = null;
            }

            output.writeObject(ServerAction.CLOSE);
            output.flush();

            return result;
        }
    }

    @Override
    protected void succeeded() {
        try {
            NotePage notePage = get();
            if(notePage == null) {
                showAlert();
                notePageController.setProcessingState(false);
            } else {
                notePageController.setCurrentNotePage(notePage);
                notePageController.setProcessingState(false);
            }
        } catch (InterruptedException e) {
            throw new AssertionError();
        } catch (ExecutionException e) {
            log.error("Failed to to download NotePage data", e);
            notePageController.setProcessingState(false);
            showAlert();
        }
    }

    @Override
    protected void failed() {
        showAlert();
        notePageController.setProcessingState(false);
    }

    private void showAlert() {
        Alert alert = new Alert(Alert.AlertType.ERROR, "Failed to download data. Please try it again later.");
        alert.showAndWait()
                .filter(response -> response == ButtonType.OK);
    }
}
