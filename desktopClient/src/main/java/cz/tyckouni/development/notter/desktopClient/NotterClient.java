package cz.tyckouni.development.notter.desktopClient;

import cz.tyckouni.development.notter.network.Entities.NoteUser;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


public class NotterClient extends Application {

    private static final Executor executor = Executors.newCachedThreadPool();

    private final static Logger log = LoggerFactory.getLogger(NotterClient.class);

    private static NoteUser noteUser;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/layouts/LoginLayout.fxml"));

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();
    }

    public static void execute(Runnable task) {
        executor.execute(task);
    }

    public static NoteUser getNoteUser() {
        return noteUser;
    }

    public static void setNoteUser(NoteUser noteUser) {
        NotterClient.noteUser = noteUser;
    }
}